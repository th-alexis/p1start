package edu.uprm.cse.datastructures.cardealer;


import java.util.ArrayList;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import edu.uprm.cse.datastructure.carservice.JsonError;
import edu.uprm.cse.datastructure.carservice.NotFoundException;
import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarList;
import edu.uprm.cse.datastructures.cardealer.util.SortedList;

@Path("/cars")
public class CarManager {
	
	private final SortedList<Car> cList = CarList.getInstance();
	
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Car[] getAllCars() {
		//If the carList is  empty then it should return a new list but empty
		//to make sure that the get works and because ultimately thats what
		//this function is suppose to do
    	if (cList.size() == 0) {
    		return new Car[1];
    	}
    	else {
    	//Else it returns the list of cars that are in the
    	//CircularSortedDoublyLinkedList of cars
    		Car[] result = new Car[cList.size()];
    		for (int i=0; i < cList.size(); ++i) {
    			result[i] = cList.get(i);
    		}
    		return result;
    	}
	}
	
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Car getCar(@PathParam("id") long id){
		//A simple loop that returns the car that the id is looking for
		//and it goes through the whole CircularSortedDoublyLinkedList of cars
		for(Car allCars : cList) {
			if(allCars.getCarId() == id) {
				return allCars;
			}
		}
		
		throw new NotFoundException(new JsonError("Error", "Customer " + id + " not found"));
	} 
	
	@POST
    @Path("/add")
    @Produces(MediaType.APPLICATION_JSON)
	public Response addCar(Car car){
		//Add the cars to the CircularSortedDoublyLinkedList of cars
		//and sorted because of the kind of list
		cList.add(car);
		return Response.status(201).build();
	}

	@PUT
	@Path("/{id}/update")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateCar(Car updCar) {
    	for (int i=0; i < cList.size(); ++i) {
    		//Makes sure that the car that we are updating has the same
    		//id like the one in the list, then it removes it and 
    		//add the car to the list
    		if (cList.get(i).getCarId() == updCar.getCarId()) {
    			cList.remove(i);
    			cList.add(updCar);
    			return Response.status(Response.Status.OK).build();
    		}
    	}
    	return Response.status(Response.Status.NOT_FOUND).build();
    }
	
	@DELETE
    @Path("/{id}/delete")
	public Response deleteCar(@PathParam("id") long id) {
    	for (int i=0; i <cList.size(); ++i) {
    		//Makes sure that the car that we are updating has the same
    		//id like the one in the list, then it removes
    		if (cList.get(i).getCarId() == id) {
    			cList.remove(i);
    			return Response.status(Response.Status.OK).build();
    		}
    	}
    	return Response.status(Response.Status.NOT_FOUND).build();
    } 
}

