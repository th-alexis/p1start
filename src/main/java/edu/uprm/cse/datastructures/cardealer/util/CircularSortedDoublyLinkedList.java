package edu.uprm.cse.datastructures.cardealer.util;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;

import edu.uprm.cse.datastructures.cardealer.model.Car;

public class CircularSortedDoublyLinkedList<E> implements SortedList<E>{
	
	//creates the nodes to use for reference when looping or changing
	//objects in the list
	@SuppressWarnings("hiding")
	private class Node<E>{

		protected E element;
		protected Node<E> next, prev;
		
		//two constructor for better use of the Node if the following functions
		public Node() {
			this.element = null;
			this.next = this.prev = null;
		}
		
		public Node(E e, Node<E> n, Node<E> p) {
			this.element = e;
			this.next = n;
			this.prev = p;
		}
		
		//getters and setter for changing references inside the list
		public E getElement() {
			return element;
		}
		public void setElement(E element) {
			this.element = element;
		}

		public Node<E> getNext() {
			return next;
		}
		public void setNext(Node<E> next) {
			this.next = next;
		}

		public Node<E> getPrev() {
			return prev;
		}
		public void setPrev(Node<E> prev) {
			this.prev = prev;
		}
	}
	
	//constructor of the CircularSortedDoublyLinkedList class
	protected Node<E> header;
	private int currentSize;
	private Comparator<E> cmp;
	
	public CircularSortedDoublyLinkedList(Comparator<E> cmp) {
		this.currentSize = 0;
		this.header = new Node<>();
		this.cmp = cmp;
	}
	
	
	//Creates the iterator for the list
	@Override
	public Iterator<E> iterator() {
		return new CircularIterator<E>();
	}
	
	@SuppressWarnings("hiding")
	private class CircularIterator<E> implements Iterator<E>{
		
		private Node<E> firstItr;
		//assigns the first element of the list so we can use it for
		//reference and comparison
		@SuppressWarnings("unchecked")
		public CircularIterator() {
			this.firstItr = (Node<E>) header.getNext();
		}
		
		//Verifies that the next node is not a null
		@Override
		public boolean hasNext() {
			return this.firstItr.getElement() != null;
		}
		
		//Gets back the element that is in the next node
		@Override
		public E next() {
			if(this.hasNext()) {
				E finalItr = this.firstItr.getElement();
				this.firstItr = this.firstItr.getNext();
				return finalItr;
			}else
				throw new NoSuchElementException();
		}
		
	}

	@Override
	public boolean add(E e) {
		//Verifies if the list is empty
		if(this.isEmpty()) {
			//Assign the new node to have the reference of the header
			//because there is just one element to be added
			Node<E> temp = new Node<E>(e, this.header, this.header);
			//updates the reference of the header
			this.header.setNext(temp);
			this.header.setPrev(temp);
			//updates the references of the new Node
			temp.setPrev(this.header);
			temp.setNext(this.header);
			//increases the size of the list
			this.currentSize++;
			return true;
		}else{
			//creates the Node to be added
			Node<E> newNode = new Node<E>(e, null, null);
			//creates a reference to be use of comparison and for updating references
			Node<E> headerPrevNode = this.header.getPrev();
			//checks if the newNode to be inserted is greater than the last item of the list
			if(cmp.compare(e, this.header.getPrev().getElement()) >= 0) {
				//creates the references of the newNode
				newNode.setNext(this.header);
				newNode.setPrev(headerPrevNode);
				//updates the references of the header and the last item
				headerPrevNode.setNext(newNode);
				this.header.setPrev(newNode);
				//increases the size of the list
				this.currentSize++;
				return true;
			}
			//creates a reference to be use of comparison and for updating references
			Node<E> currentNode = this.header.getNext();
			//checks if the newNode to be inserted is lesser than the first item in the list
			if(cmp.compare(e, currentNode.getElement()) <= 0) {
				//updates the references of the header and the first item
				this.header.setNext(newNode);
				currentNode.setPrev(newNode);
				//creates the references of the newNode
				newNode.setNext(currentNode);
				newNode.setPrev(this.header);
				//increases the size of the list
				this.currentSize++;
				return true;
			}
			//a loop to iterate through the whole list using the currentNode
			//as a reference and a stop point for the infinite loop
			while(currentNode.getElement() != null) {
				//gets the next node, of the currentNode for example if the list has
				//L=[H,1,2,3] then the current node should be 2
				currentNode = currentNode.getNext();
				//gets the previous node, of the currentNode for example if the list has
				//L=[H,1,2,3] then the previous node should be 1
				Node<E> prevNode = currentNode.getPrev();
				//checks if the node to be inserted is to be put on the middle of two nodes
				//specifically if the newNode is lesser than the currentNode and therefore
				//greater than the previous node
				if(cmp.compare(e, currentNode.getElement()) <= 0) {
					//updates the references of the currentNode and the prevNode
					prevNode.setNext(newNode);
					currentNode.setPrev(newNode);
					//creates the references of the newNode
					newNode.setNext(currentNode);
					newNode.setPrev(prevNode);
					//increases the size of the list
					this.currentSize++;
					return true;
				}
			}
		}
		return false;	
	}

	@Override
	public int size() {
		return this.currentSize;
	}

	@Override
	public boolean remove(E e) {
		int i = this.firstIndex(e);
		//checks first if it can remove the element, otherwise it will just
		//find it and delete it
		if(i<0)
			return false;
		else {
			this.remove(i);
			return true;
		}
	}

	@Override
	public boolean remove(int index) {
		//checks first if the index is acceptable 
		if ((index < 0) || (index >= this.currentSize))
			throw new IndexOutOfBoundsException();
		else {
			//creates a tempNode for comparison and as reference
			Node<E> temp = this.header;
			int currentPosition = 0;
			//creates the Node to be removed
			Node<E> target = null;
			//iterates through the whole list until the index to be removed
			//is found
			while(currentPosition != index) {
				temp= temp.getNext();
				currentPosition++;
			}
			//gets the target to be removed
			target = temp.getNext();
			//updates the references of the preNode and the nextNode
			temp.setNext(target.getNext());
			target.getNext().setPrev(temp);
			//Ultimately removes the element of the list
			target.setElement(null);
			target.setNext(null);
			target.setPrev(null);
			//decreases the list 
			this.currentSize--;
			return true;
		}
	}

	@Override
	public int removeAll(E e) {
		int count = 0;
		//uses the remove function to help and remove every element that 
		//is equal to the one given and keeps a count
		while(this.remove(e))
			count++;
		
		return count;
	}

	@Override
	public E first() {
		//gets the first element of the list
		return header.getNext().getElement();
	}

	@Override
	public E last() {
		//gets the last element of the list
		return header.getPrev().getElement();
	}

	@Override
	public E get(int index) {
		//uses the helper  getPosition to get the element in given index 
		Node<E> temp = this.getPosition(index);
		return temp.getElement();
		
	}
	
	//helper function to aid the get function by finding the position
	//the Node is in
	private Node<E> getPosition(int index){
		//creates a tempNode for comparison and as reference
		int currentPosition = 0;
		Node<E> temp = this.header.getNext();
		//iterates through the whole list until it finds the element we
		//want and keeping a count of such movements inside the list
		while(currentPosition != index) {
			temp = temp.getNext();
			currentPosition++;
		}
		//then sends the index the element is on
		return temp;
	}

	@Override
	public void clear() {
		//remove the first element of the list until the list is completely empty
		while(!this.isEmpty())
			this.remove(0);
	}

	@Override
	public boolean contains(E e) {
		//uses the iterator function to iterate through the list
		Iterator<E> iter = new CircularIterator<E>();
		E iterElement;
		
		while(iter.hasNext()) {
			iterElement = iter.next();
			//then when it finds that element return true because that element
			//is a member of that list
			if(iterElement.equals(e))
				return true;
		}
		return false;
	}

	@Override
	public boolean isEmpty() {
		//if the size of the list if 0 then the list is empty
		return this.size() == 0;
	}

	@Override
	public int firstIndex(E e) {
		int count = 0;
		//uses the iterator function to iterate through the list
		Iterator<E> iter = new CircularIterator<E>();
		E iterElement;
		
		while(iter.hasNext()) {
			iterElement = iter.next();
			//if it finds the element given it will return the count of iterations
			//that the iterator had to went through to be equal to the element given
			if(iterElement.equals(e))
				return count;
			
			count++;
		}
		//If not found
		return -1;
	}

	@Override
	public int lastIndex(E e) {
		int count = 0, result = 0;
		//uses the iterator function to iterate through the list
		Iterator<E> iter = new CircularIterator<E>();
		E iterElement;
		
		while(iter.hasNext()) {
			iterElement = iter.next();
			//if it finds the element given it will return the count of iterations
			//that the iterator had to went through to be equal to the element given
			//but by finding only the last time it found that kind of element
			if(iterElement.equals(e))
				result = count;
			
			count++;
		}
		//If not found
		if(result == 0)
			return -1;
		else
			return result;
	}



}
