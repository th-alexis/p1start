package edu.uprm.cse.datastructures.cardealer.model;

import java.util.Comparator;

public class CarComparator implements Comparator<Car>{

	//Basically this goes through 3 filters so the Car can be compare
	//correctly, each step checks if the previous compare was done
	
	@Override
	public int compare(Car o1, Car o2) {
		
		//After the CarBrand and the CarModel are sorted, sort by CarModelOPtion
		if(o1.getCarModel().compareTo(o2.getCarModel()) == 0 &&
				o1.getCarBrand().compareTo(o2.getCarBrand()) == 0)
			return o1.getCarModelOption().compareTo(o2.getCarModelOption());
		
		//After the CarBrand was Sorted, we check for the CraBrand
		if(o1.getCarBrand().compareTo(o2.getCarBrand()) == 0)
			return o1.getCarModel().compareTo(o2.getCarModel());
		
		//This is basically the first compare that is going to do every time
		return o1.getCarBrand().compareTo(o2.getCarBrand());
	}
}
